# Marp - Markdown Presentation Writer Packaging

Create Marp Debian packages from zip file.

Releases can be found in [builds](https://gitlab.com/packaging/marp/pipelines) and will be pushed to [my repo](https://repo.morph027.de/).

![](marp-package-desktop-integration.png)

## Requirements

* curl
* ruby + gem + fpm (```gem install fpm```)
* bsdtar

## Usage

Just run the create script ```.gitlab-ci/create``` and it downloads the release zip file from Github and will create a deb package. As fpm is able to build other packages too, you might adjust ```-t deb``` in function ```create_deb``` with another type e.g. _rpm_.
