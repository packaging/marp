#!/bin/bash

set -e

PREREQS=( "bsdtar" "curl" "fpm" "jq")

_check_prereq() {
  local PACKAGE=$1
  which $PACKAGE >/dev/null || (echo "$PACKAGE is missing" && exit 1)
}

for PREREQ in ${PREREQS[@]}
do
  _check_prereq $PREREQ
done

_build_from_github(){
  _download_marp() {
    local URL="https://github.com/yhatt/marp/releases/download/v${CI_COMMIT_TAG%%-*}/${CI_COMMIT_TAG%%-*}-Marp-linux-x64.tar.gz"
    curl -s --head --fail --connect-timeout 2 "$URL" -o /dev/null || (echo "${URL} does not exist" && exit 1)
    mkdir -p package_root/opt/marp
    curl -sL ${URL} | bsdtar -C package_root/opt/marp -xf-
    find package_root/opt/marp -type d -exec chmod +x {} \+
    chmod +x package_root/opt/marp/Marp
    curl -sL https://raw.githubusercontent.com/yhatt/marp/master/images/marp.png -o package_root/opt/marp/marp.png
  }

  _create_package() {
    fpm -s dir -t deb -n marp \
    --version "${CI_COMMIT_TAG%%-*}" \
    --after-install scripts/after-install \
    --maintainer "Stefan Heitmülller <stefan.heitmueller@gmx.com>" \
    --url "https://yhatt.github.io/marp/" \
    --description "Markdown Presentation Writer" \
    -C package_root \
    opt tmp
  }

  _cleanup() {
    rm -rf package_root/opt/marp
  }

  _download_marp
  _create_package
  _cleanup
}

_build_from_github