#!/bin/sh

echo "$SIGNING_KEY" > /tmp/key
gpg --import /tmp/key

find "${CI_PROJECT_DIR}" -type f -name '*.deb' -exec \
	deb-s3 upload \
		--bucket $BUCKET \
		--endpoint $S3 \
		--arch amd64 \
		--codename $CODENAME \
		--access-key-id=$ACCESS_KEY \
		--secret-access-key=$SECRET_KEY \
		--force-path-style \
		--sign=$SIGNING_KEY_ID \
	{} \+